FROM ubuntu:22.04
MAINTAINER Thibaud Labat

# Install managed dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
  autoconf bison flex g++ graphviz libpcre3 libpcre3-dev \
  libcurl4 libcurl4-openssl-dev make openjdk-8-jdk-headless \
  vim wget xutils-dev dos2unix unixodbc-dev libmysqlclient-dev \
  texlive-font-utils ghostscript \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

# Copy jni_md.h to new location (this resolvfes file not found warning for XSB)
WORKDIR /usr/lib/jvm/java-8-openjdk-amd64/include
RUN cp linux/jni_md.h .

# Set JAVA_HOME for XSB
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64

# Install XSB
WORKDIR /root
RUN wget https://downloads.sourceforge.net/project/xsb/xsb/5.0%20%28Green%20Tea%29/XSB-5.0.tar.gz
RUN tar -xzf XSB-5.0.tar.gz && rm XSB-5.0.tar.gz

WORKDIR /root/XSB/build
RUN ./configure && ./makexsb
ENV PATH ${PATH}:/root/XSB/bin/

# Install MulVAL
WORKDIR /root
RUN mkdir mulval
COPY . mulval

WORKDIR /root/mulval
ENV MULVALROOT /root/mulval/
ENV PATH ${PATH}:${MULVALROOT}/bin:${MULVALROOT}/utils
RUN make

# Create the input directory
WORKDIR /root

# Run bash
CMD bash